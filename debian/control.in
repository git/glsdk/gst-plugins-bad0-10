Source: gst-plugins-bad@GST_ABI@
Section: libs
Priority: extra
Maintainer: Maintainers of GStreamer packages <pkg-gstreamer-maintainers@lists.alioth.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Sjoerd Simons <sjoerd@debian.org>
Build-Depends: BUILDDEPS
Standards-Version: 3.8.4
Vcs-Git: git://gitorious.org/ubuntu-omap/gst-plugins-bad0-10.git
Vcs-Browser: https://gitorious.org/ubuntu-omap/gst-plugins-bad0-10
Homepage: http://gstreamer.freedesktop.org/modules/gst-plugins-bad.html

Package: @GST_PKGNAME@-plugins-bad-doc
Architecture: armhf
Section: doc
Depends: ${misc:Depends},
         gstreamer0.10-doc,
         gstreamer0.10-plugins-base-doc
Replaces: gstreamer0.10-plugins-bad (<< 0.10.5-3)
Description: GStreamer documentation for plugins from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains the documentation for plugins from the "bad" set.
 
Package: @GST_PKGNAME@-plugins-bad
Architecture: armhf
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
         gstreamer0.10-plugins-base,
         libgstreamer-plugins-bad@GST_DEB_ABI@ (= ${binary:Version}),
Replaces: gstreamer0.10-plugins-bad-multiverse (<< 0.10.5-2),
          gstreamer0.10-plugins-really-bad (<< 0.10.14),
          gstreamer0.10-plugins-farsight,
          gstreamer0.10-schroedinger,
          gstreamer0.10-xvid
Provides: gstreamer0.10-plugins-farsight,
          gstreamer0.10-schroedinger,
          ${gstreamer:Provides}
Conflicts: gstreamer0.10-plugins-ugly (<< 0.10.13.2),
           gstreamer0.10-plugins-good (<< 0.10.15.2),
           gstreamer0.10-plugins-farsight,
           gstreamer0.10-schroedinger,
           gstreamer0.10-xvid
Suggests: frei0r-plugins
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Description: GStreamer plugins from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.

Package: @GST_PKGNAME@-sdl
Architecture: armhf
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Replaces: gstreamer0.10-plugins-bad (<< 0.10.5-3)
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Provides: ${gstreamer:Provides}
Description: GStreamer plugin for SDL output
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GStreamer plugin for SDL, the Simple
 DirectMedia Layer.

Package: @GST_PKGNAME@-plugins-bad-dbg
Architecture: armhf
Section: debug
Priority: extra
Depends: @GST_PKGNAME@-plugins-bad (= ${binary:Version}),
         @GST_PKGNAME@-sdl (= ${binary:Version}),
         ${misc:Depends}
Description: GStreamer plugins from the "bad" set (debug symbols)
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains unstripped shared libraries. It is provided primarily
 to provide a backtrace with names in a debugger, this makes it somewhat
 easier to interpret core dumps. The libraries are installed in
 /usr/lib/debug and are automatically used by gdb.

Package: libgstreamer-plugins-bad@GST_DEB_ABI@
Architecture: armhf
Section: libs
Priority: extra
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: @GST_PKGNAME@-plugins-bad (<< 0.10.22.3-2)
Replaces: @GST_PKGNAME@-plugins-bad (<< 0.10.22.3-2)
Description: GStreamer development files for libraries from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains shared GStreamer libraries from the "bad" set. The API
 is not guaranteed to be stable.

Package: libgstreamer-plugins-bad@GST_ABI@-dev
Architecture: armhf
Section: libdevel
Priority: extra
Depends: ${misc:Depends},
	 libgstreamer-plugins-bad@GST_DEB_ABI@ (= ${binary:Version}),
Description: GStreamer development files for libraries from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains development files for GStreamer libraries from the
 "bad" set. The API is not guaranteed to be stable.

