/*
 * GStreamer Wayland video sink
 *
 * Copyright (C) 2011 Intel Corporation
 * Copyright (C) 2011 Sreerenj Balachandran <sreerenj.balachandran@intel.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/**
 * SECTION:element-waylandsink
 *
 *  The waylandsink is creating its own window and render the decoded video frames to that.
 *  Setup the Wayland environment as described in
 *  <ulink url="http://wayland.freedesktop.org/building.html">Wayland</ulink> home page.
 *  The current implementaion is based on weston compositor. 
 *
 * <refsect2>
 * <title>Example pipelines</title>
 * |[
 * gst-launch -v videotestsrc ! waylandsink
 * ]| test the video rendering in wayland
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gstwaylandsink.h"
#include "gstwlbufferpriv.h"

#include <wayland-client-protocol.h>
#include "wayland-drm-client-protocol.h"

#include <linux/input.h>

/* signals */
enum
{
  SIGNAL_0,
  SIGNAL_FRAME_READY,
  LAST_SIGNAL
};

/* Properties */
enum
{
  PROP_0,
  PROP_WAYLAND_DISPLAY
};

GST_DEBUG_CATEGORY (gstwayland_debug);

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw-yuv, "
        "format = (fourcc){NV12, I420, YUY2, UYVY}, "
        "width = " GST_VIDEO_SIZE_RANGE ", "
        "height = " GST_VIDEO_SIZE_RANGE ", "
        "framerate = " GST_VIDEO_FPS_RANGE));
/*Fixme: Add more interfaces */
GST_BOILERPLATE (GstWaylandSink, gst_wayland_sink, GstVideoSink,
    GST_TYPE_VIDEO_SINK);


static void gst_wayland_sink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void gst_wayland_sink_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_wayland_sink_finalize (GObject * object);
static GstCaps *gst_wayland_sink_get_caps (GstBaseSink * bsink);
static gboolean gst_wayland_sink_set_caps (GstBaseSink * bsink, GstCaps * caps);
static gboolean gst_wayland_sink_start (GstBaseSink * bsink);
static gboolean gst_wayland_sink_stop (GstBaseSink * bsink);
static GstFlowReturn
gst_wayland_sink_buffer_alloc (GstBaseSink * bsink, guint64 offset,
    guint size, GstCaps * caps, GstBuffer ** buf);
static gboolean gst_wayland_sink_preroll (GstBaseSink * bsink,
    GstBuffer * buffer);
static gboolean gst_wayland_sink_render (GstBaseSink * bsink,
    GstBuffer * buffer);

static struct display *create_display (void);
static void registry_handle_global (void *data, struct wl_registry *registry,
    uint32_t id, const char *interface, uint32_t version);
static void redraw (void *data, struct wl_callback *callback, uint32_t time);
static void create_window (GstWaylandSink * sink, struct display *display,
    int width, int height);
static void input_grab (struct input *input, struct window *window);
static void input_ungrab (struct input *input);

static void
gst_wayland_sink_base_init (gpointer gclass)
{

  GstElementClass *element_class = GST_ELEMENT_CLASS (gclass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_template));

  gst_element_class_set_details_simple (element_class,
      "wayland video sink", "Sink/Video",
      "Output to wayland surface",
      "Sreerenj Balachandran <sreerenj.balachandran@intel.com>");
}

static void
gst_wayland_sink_class_init (GstWaylandSinkClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseSinkClass *gstbasesink_class;

  gobject_class = (GObjectClass *) klass;
  gstbasesink_class = (GstBaseSinkClass *) klass;

  gobject_class->set_property = gst_wayland_sink_set_property;
  gobject_class->get_property = gst_wayland_sink_get_property;
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_wayland_sink_finalize);

  gstbasesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_wayland_sink_get_caps);
  gstbasesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_wayland_sink_set_caps);
  gstbasesink_class->start = GST_DEBUG_FUNCPTR (gst_wayland_sink_start);
  gstbasesink_class->buffer_alloc =
      GST_DEBUG_FUNCPTR (gst_wayland_sink_buffer_alloc);
  gstbasesink_class->stop = GST_DEBUG_FUNCPTR (gst_wayland_sink_stop);
  gstbasesink_class->preroll = GST_DEBUG_FUNCPTR (gst_wayland_sink_preroll);
  gstbasesink_class->render = GST_DEBUG_FUNCPTR (gst_wayland_sink_render);

  g_object_class_install_property (gobject_class, PROP_WAYLAND_DISPLAY,
      g_param_spec_pointer ("wayland-display", "Wayland Display",
          "Wayland  Display handle created by the application ",
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  parent_class = g_type_class_peek_parent (klass);
}

static void
gst_wayland_sink_init (GstWaylandSink * sink,
    GstWaylandSinkClass * wayland_sink_class)
{

  sink->caps = NULL;
  sink->display = NULL;
  sink->window = NULL;

  sink->wayland_lock = g_mutex_new ();
}

static void
gst_wayland_sink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (object);

  switch (prop_id) {
    case PROP_WAYLAND_DISPLAY:
      g_value_set_pointer (value, sink->display);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_wayland_sink_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (object);

  switch (prop_id) {
    case PROP_WAYLAND_DISPLAY:
      sink->display = g_value_get_pointer (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
input_grab (struct input *input, struct window *window)
{
  input->grab = window;
}

static void
input_ungrab (struct input *input)
{
  input->grab = NULL;
}

static void
input_remove_pointer_focus (struct input *input)
{
  struct window *window = input->pointer_focus;

  if (!window)
    return;

  input->pointer_focus = NULL;
}

static void
input_destroy (struct input *input)
{
  input_remove_pointer_focus (input);

  if (input->display->seat_version >= 3) {
    if (input->pointer)
      wl_pointer_release (input->pointer);
  }

  wl_list_remove (&input->link);
  wl_seat_destroy (input->seat);
  free (input);
}

static void
display_destroy_inputs (struct display *display)
{
  struct input *tmp;
  struct input *input;

  wl_list_for_each_safe (input, tmp, &display->input_list, link)
      input_destroy (input);
}

static void
destroy_display (struct display *display)
{
  if (display->drm)
    wl_drm_destroy (display->drm);

  if (display->shell)
    wl_shell_destroy (display->shell);

  if (display->compositor)
    wl_compositor_destroy (display->compositor);

  display_destroy_inputs (display);
  wl_display_flush (display->display);
  wl_display_disconnect (display->display);
  free (display);
}

static void
destroy_window (struct window *window)
{
  if (window->shell_surface)
    wl_shell_surface_destroy (window->shell_surface);
  if (window->surface)
    wl_surface_destroy (window->surface);
  if (window->opaque_region)
    wl_region_destroy (window->opaque_region);
  free (window);
}

static void
gst_wayland_sink_finalize (GObject * object)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (object);

  GST_DEBUG_OBJECT (sink, "Finalizing the sink..");

  if (sink->window)
    destroy_window (sink->window);
  if (sink->display)
    destroy_display (sink->display);

  if (sink->pool) {
    gst_drm_buffer_pool_destroy (sink->pool);
    sink->pool = NULL;
  }
  gst_buffer_replace (&sink->last_buf, NULL);
  gst_buffer_replace (&sink->display_buf, NULL);

  g_mutex_free (sink->wayland_lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GstCaps *
gst_wayland_sink_get_caps (GstBaseSink * bsink)
{
  return gst_caps_copy (gst_static_pad_template_get_caps (&sink_template));
}

static void
drm_handle_device (void *data, struct wl_drm *drm, const char *device)
{
  struct display *d = data;
  drm_magic_t magic;

  d->fd = open (device, O_RDWR | O_CLOEXEC);
  if (d->fd == -1) {
    GST_ERROR ("could not open %s: %m", device);
    // XXX hmm, probably need to throw up some error now??
    return;
  }

  drmGetMagic (d->fd, &magic);
  wl_drm_authenticate (d->drm, magic);
}


static void
drm_handle_format (void *data, struct wl_drm *drm, uint32_t format)
{
  struct display *d = data;
  GST_DEBUG ("got format: %" GST_FOURCC_FORMAT, GST_FOURCC_ARGS (format));
  d->formats[d->format_count++] = format;
}

static void
drm_handle_authenticated (void *data, struct wl_drm *drm)
{
  struct display *d = data;
  GST_DEBUG ("authenticated");
  d->dev = omap_device_new (d->fd);
  d->authenticated = 1;
}

static const struct wl_drm_listener drm_listener = {
  drm_handle_device,
  drm_handle_format,
  drm_handle_authenticated
};

static void
pointer_handle_enter (void *data, struct wl_pointer *pointer,
    uint32_t serial, struct wl_surface *surface,
    wl_fixed_t sx_w, wl_fixed_t sy_w)
{
  struct input *input = data;


  if (!surface) {
    /* enter event for a window we've just destroyed */
    return;
  }

  input->display->serial = serial;
  input->pointer_focus = wl_surface_get_user_data (surface);

}

static void
pointer_handle_leave (void *data, struct wl_pointer *pointer,
    uint32_t serial, struct wl_surface *surface)
{
  struct input *input = data;

  input_remove_pointer_focus (input);
}

static void
pointer_handle_motion (void *data, struct wl_pointer *pointer,
    uint32_t time, wl_fixed_t sx_w, wl_fixed_t sy_w)
{
  struct input *input = data;
  struct window *window = input->pointer_focus;

  if (!window)
    return;


  if (input->grab)
    wl_shell_surface_move (input->grab->shell_surface, input->seat,
        input->display->serial);

}

static void
pointer_handle_button (void *data, struct wl_pointer *pointer, uint32_t serial,
    uint32_t time, uint32_t button, uint32_t state_w)
{
  struct input *input = data;
  enum wl_pointer_button_state state = state_w;
  input->display->serial = serial;

  if (button == BTN_LEFT) {
    if (state == WL_POINTER_BUTTON_STATE_PRESSED)
      input_grab (input, input->pointer_focus);

    if (input->grab && state == WL_POINTER_BUTTON_STATE_RELEASED)
      input_ungrab (input);
  }

  if (input->grab)
    wl_shell_surface_move (input->grab->shell_surface, input->seat,
        input->display->serial);
}

static void
pointer_handle_axis (void *data, struct wl_pointer *pointer,
    uint32_t time, uint32_t axis, wl_fixed_t value)
{
}

static const struct wl_pointer_listener pointer_listener = {
  pointer_handle_enter,
  pointer_handle_leave,
  pointer_handle_motion,
  pointer_handle_button,
  pointer_handle_axis,
};

static void
touch_handle_down (void *data, struct wl_touch *wl_touch,
    uint32_t serial, uint32_t time, struct wl_surface *surface,
    int32_t id, wl_fixed_t x_w, wl_fixed_t y_w)
{
  struct input *input = data;
  struct touch_point *tp;

  input->display->serial = serial;
  input->touch_focus = wl_surface_get_user_data (surface);
  if (!input->touch_focus) {
    return;
  }

  tp = malloc (sizeof *tp);
  if (tp) {
    tp->id = id;
    wl_list_insert (&input->touch_point_list, &tp->link);
    wl_shell_surface_move (input->touch_focus->shell_surface, input->seat,
        serial);
  }
}

static void
touch_handle_motion (void *data, struct wl_touch *wl_touch,
    uint32_t time, int32_t id, wl_fixed_t x_w, wl_fixed_t y_w)
{
  struct input *input = data;
  struct touch_point *tp;


  if (!input->touch_focus) {
    return;
  }
  wl_list_for_each (tp, &input->touch_point_list, link) {
    if (tp->id != id)
      continue;

    wl_shell_surface_move (input->touch_focus->shell_surface, input->seat,
        input->display->serial);

    return;
  }
}

static void
touch_handle_frame (void *data, struct wl_touch *wl_touch)
{
}

static void
touch_handle_cancel (void *data, struct wl_touch *wl_touch)
{
}

static void
touch_handle_up (void *data, struct wl_touch *wl_touch,
    uint32_t serial, uint32_t time, int32_t id)
{
  struct input *input = data;
  struct touch_point *tp, *tmp;

  if (!input->touch_focus) {
    return;
  }

  wl_list_for_each_safe (tp, tmp, &input->touch_point_list, link) {
    if (tp->id != id)
      continue;

    wl_list_remove (&tp->link);
    free (tp);

    return;
  }
}

static const struct wl_touch_listener touch_listener = {
  touch_handle_down,
  touch_handle_up,
  touch_handle_motion,
  touch_handle_frame,
  touch_handle_cancel,
};



static void
seat_handle_capabilities (void *data, struct wl_seat *seat,
    enum wl_seat_capability caps)
{
  struct input *input = data;

  if ((caps & WL_SEAT_CAPABILITY_POINTER) && !input->pointer) {
    input->pointer = wl_seat_get_pointer (seat);
    wl_pointer_set_user_data (input->pointer, input);
    wl_pointer_add_listener (input->pointer, &pointer_listener, input);
  } else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && input->pointer) {
    wl_pointer_destroy (input->pointer);
    input->pointer = NULL;
  }

  if ((caps & WL_SEAT_CAPABILITY_TOUCH) && !input->touch) {
    input->touch = wl_seat_get_touch (seat);
    wl_touch_set_user_data (input->touch, input);
    wl_touch_add_listener (input->touch, &touch_listener, input);
  } else if (!(caps & WL_SEAT_CAPABILITY_TOUCH) && input->touch) {
    wl_touch_destroy (input->touch);
    input->touch = NULL;
  }
}

static void
seat_handle_name (void *data, struct wl_seat *seat, const char *name)
{

}

static const struct wl_seat_listener seat_listener = {
  seat_handle_capabilities,
  seat_handle_name
};

static void
display_add_input (struct display *d, uint32_t id)
{
  struct input *input;

  input = calloc (1, sizeof (*input));
  if (input == NULL) {
    fprintf (stderr, "%s: out of memory\n", "gst-wayland-sink");
    exit (EXIT_FAILURE);
  }
  input->display = d;
  input->seat = wl_registry_bind (d->registry, id, &wl_seat_interface,
      MAX (d->seat_version, 3));
  input->touch_focus = NULL;
  input->pointer_focus = NULL;
  wl_list_init (&input->touch_point_list);
  wl_list_insert (d->input_list.prev, &input->link);

  wl_seat_add_listener (input->seat, &seat_listener, input);
  wl_seat_set_user_data (input->seat, input);

}

static void
registry_handle_global (void *data, struct wl_registry *registry,
    uint32_t id, const char *interface, uint32_t version)
{
  struct display *d = data;

  GST_DEBUG ("global: %s", interface);

  if (strcmp (interface, "wl_compositor") == 0) {
    d->compositor =
        wl_registry_bind (registry, id, &wl_compositor_interface, 1);
  } else if (strcmp (interface, "wl_shell") == 0) {
    d->shell = wl_registry_bind (registry, id, &wl_shell_interface, 1);
  } else if (strcmp (interface, "wl_drm") == 0) {
    d->drm = wl_registry_bind (registry, id, &wl_drm_interface, 1);
    wl_drm_add_listener (d->drm, &drm_listener, d);
  } else if (strcmp (interface, "wl_seat") == 0) {
    d->seat_version = version;
    display_add_input (d, id);
  }

}

static const struct wl_registry_listener registry_listener = {
  registry_handle_global,
  NULL
};

static struct display *
create_display (void)
{
  struct display *display;

  display = calloc (1, sizeof *display);
  display->display = wl_display_connect (NULL);

  if (display->display == NULL) {
    free (display);
    return NULL;
  }

  wl_list_init (&display->input_list);
  display->registry = wl_display_get_registry (display->display);
  wl_registry_add_listener (display->registry, &registry_listener, display);

  wl_display_roundtrip (display->display);

  if (!display->drm) {
    GST_ERROR ("No wl_drm global received");
    return NULL;
  }

  return display;
}

static void
wait_authentication (GstWaylandSink * sink)
{
  while (!sink->display->authenticated) {
    GST_DEBUG_OBJECT (sink, "waiting for authentication");
    wl_display_roundtrip (sink->display->display);
  }
}


static void
create_pool (GstWaylandSink * sink, GstCaps * caps)
{

  GstVideoFormat format;
  gint width, height, size;

  wait_authentication (sink);

  while (!sink->display->authenticated) {
    GST_DEBUG_OBJECT (sink, "not authenticated yet");
  }

  if (sink->pool) {
    GST_INFO_OBJECT (sink, "recreating pool");
    gst_drm_buffer_pool_destroy (sink->pool);
    sink->pool = NULL;
  }

  gst_video_format_parse_caps (caps, &format, &width, &height);
  size = gst_video_format_get_size (format, width, height);

  sink->pool = gst_drm_buffer_pool_new (GST_ELEMENT (sink),
      sink->display->fd, caps, size);
}

static GstFlowReturn
gst_wayland_sink_buffer_alloc (GstBaseSink * bsink, guint64 offset, guint size,
    GstCaps * caps, GstBuffer ** buf)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (bsink);
  GstFlowReturn ret = GST_FLOW_OK;
  GST_DEBUG_OBJECT (sink, "begin");

  if (G_UNLIKELY (!caps)) {
    GST_WARNING_OBJECT (sink, "have no caps, doing fallback allocation");
    *buf = NULL;
    ret = GST_FLOW_OK;
    goto beach;
  }

  GST_LOG_OBJECT (sink,
      "a buffer of %d bytes was requested with caps %" GST_PTR_FORMAT
      " and offset %" G_GUINT64_FORMAT, size, caps, offset);

  /* initialize the buffer pool if not initialized yet */
  if (!sink->pool || (gst_drm_buffer_pool_size (sink->pool) != size))
    create_pool (sink, caps);

  *buf = GST_BUFFER_CAST (gst_drm_buffer_pool_get (sink->pool, FALSE));

beach:

  return ret;
}

static gboolean
gst_wayland_sink_set_caps (GstBaseSink * bsink, GstCaps * caps)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (bsink);
  GstCaps *allowed_caps;
  gboolean ret = TRUE;
  gint width, height;
  GstVideoFormat format;

  GST_LOG_OBJECT (sink, "set caps %" GST_PTR_FORMAT, caps);

  allowed_caps = gst_pad_get_caps (GST_BASE_SINK_PAD (bsink));

  if (!gst_caps_can_intersect (allowed_caps, caps))
    return FALSE;

  ret = gst_video_format_parse_caps (caps, &format, &width, &height);
  if (!ret)
    return FALSE;
  if (width <= 0 || height <= 0) {
    GST_ELEMENT_ERROR (sink, CORE, NEGOTIATION, (NULL),
        ("Invalid image size."));
    return FALSE;
  }

  sink->video_width = width;
  sink->video_height = height;


  gst_caps_replace (&sink->caps, caps);

  if (!sink->pool || !gst_drm_buffer_pool_check_caps (sink->pool, caps))
    create_pool (sink, caps);

  return TRUE;
}


static void
redraw (void *data, struct wl_callback *callback, uint32_t time)
{
  GstWaylandSink *sink = (GstWaylandSink *) data;
  GST_LOG_OBJECT (sink, "render finished");
  wl_callback_destroy (sink->callback);
  sink->callback = NULL;
}

static const struct wl_callback_listener frame_listener = {
  redraw
};

static void
handle_ping (void *data, struct wl_shell_surface *shell_surface,
    uint32_t serial)
{
  wl_shell_surface_pong (shell_surface, serial);
}

static void
handle_configure (void *data, struct wl_shell_surface *shell_surface,
    uint32_t edges, int32_t width, int32_t height)
{
}

static void
handle_popup_done (void *data, struct wl_shell_surface *shell_surface)
{
}

static const struct wl_shell_surface_listener shell_surface_listener = {
  handle_ping,
  handle_configure,
  handle_popup_done
};

static void
create_window (GstWaylandSink * sink, struct display *display, int width,
    int height)
{
  struct window *window;

  if (sink->window)
    return;

  g_mutex_lock (sink->wayland_lock);

  window = malloc (sizeof *window);
  window->display = display;
  window->width = width;
  window->height = height;
  window->surface = wl_compositor_create_surface (display->compositor);
  wl_surface_set_user_data (window->surface, window);

  /* the video area should be opaque: */
  window->opaque_region = wl_compositor_create_region (display->compositor);
  wl_region_add (window->opaque_region, 0, 0, width, height);

  window->shell_surface = wl_shell_get_shell_surface (display->shell,
      window->surface);
  wl_shell_surface_add_listener (window->shell_surface, &shell_surface_listener,
      sink);
  wl_shell_surface_set_toplevel (window->shell_surface);

#if 0                           /* XXX maybe we want a property for fullscreen? */

  wl_shell_surface_set_fullscreen (window->shell_surface,
      WL_SHELL_SURFACE_FULLSCREEN_METHOD_SCALE, 0, NULL);
#endif

  sink->window = window;

  g_mutex_unlock (sink->wayland_lock);
}

static gboolean
gst_wayland_sink_start (GstBaseSink * bsink)
{
  GstWaylandSink *sink = (GstWaylandSink *) bsink;
  gboolean result = TRUE;

  GST_DEBUG_OBJECT (sink, "start");

  if (!sink->display)
    sink->display = create_display ();

  if (sink->display == NULL) {
    GST_ELEMENT_ERROR (bsink, RESOURCE, OPEN_READ_WRITE,
        ("Could not initialise Wayland output"),
        ("Could not create Wayland display"));
    return FALSE;
  }

  return result;
}

static gboolean
gst_wayland_sink_stop (GstBaseSink * bsink)
{
  GstWaylandSink *sink = (GstWaylandSink *) bsink;

  GST_DEBUG_OBJECT (sink, "stop");
  gst_buffer_replace (&sink->last_buf, NULL);
  gst_buffer_replace (&sink->display_buf, NULL);

  return TRUE;
}

static GstFlowReturn
gst_wayland_sink_preroll (GstBaseSink * bsink, GstBuffer * buffer)
{
  GST_DEBUG_OBJECT (bsink, "preroll buffer %p, data = %p", buffer,
      GST_BUFFER_DATA (buffer));
  return gst_wayland_sink_render (bsink, buffer);
}

static GstFlowReturn
gst_wayland_sink_render (GstBaseSink * bsink, GstBuffer * inbuf)
{
  GstWaylandSink *sink = GST_WAYLAND_SINK (bsink);
  GstBuffer *buf = NULL;

  GstVideoRectangle src, dst, res;
  GstWLBufferPriv *priv;
  struct window *window;


  GST_LOG_OBJECT (sink,
      "render buffer %p, data = %p, timestamp = %" GST_TIME_FORMAT, inbuf,
      GST_BUFFER_DATA (inbuf), GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (inbuf)));

  if (!sink->window) {
    gint video_width = sink->video_width;
    gint video_height = sink->video_height;
    GstVideoCrop *crop = gst_buffer_get_video_crop (inbuf);
    if (crop) {
      video_width = gst_video_crop_width (crop);
      video_height = gst_video_crop_height (crop);
    }
    create_window (sink, sink->display, video_width, video_height);
  }
  window = sink->window;

  wait_authentication (sink);

  priv = gst_wl_buffer_priv (sink, inbuf);
  if (priv) {
    buf = gst_buffer_ref (inbuf);
  } else {
    GST_LOG_OBJECT (sink, "not a DRM buffer, slow-path!");
    buf = gst_drm_buffer_pool_get (sink->pool, FALSE);
    if (buf) {
      GST_BUFFER_TIMESTAMP (buf) = GST_BUFFER_TIMESTAMP (inbuf);
      GST_BUFFER_DURATION (buf) = GST_BUFFER_DURATION (inbuf);
      memcpy (GST_BUFFER_DATA (buf),
          GST_BUFFER_DATA (inbuf), GST_BUFFER_SIZE (inbuf));
      priv = gst_wl_buffer_priv (sink, buf);
    }
  }

  if (!priv) {
    GST_ERROR_OBJECT (sink, "cannot render frame");
    return GST_FLOW_ERROR;
  }

  g_warn_if_fail (!sink->callback);
  window->buffer = priv->buffer;
  src.w = sink->video_width;
  src.h = sink->video_height;
  dst.w = window->width;
  dst.h = window->height;

  gst_video_sink_center_rect (src, dst, &res, FALSE);

  GST_DEBUG_OBJECT (sink, "render: %d,%d %dx%d", res.x, res.y, res.w, res.h);

  wl_surface_attach (window->surface, window->buffer, res.x, res.y);
  wl_surface_set_opaque_region (window->surface, window->opaque_region);
  wl_surface_damage (window->surface, 0, 0, res.w, res.h);


  if (sink->callback)
    wl_callback_destroy (sink->callback);

  sink->callback = wl_surface_frame (window->surface);
  wl_callback_add_listener (sink->callback, &frame_listener, sink);
  wl_surface_commit (sink->window->surface);
  wl_display_flush (sink->display->display);
  gst_buffer_replace (&sink->last_buf, sink->display_buf);
  gst_buffer_replace (&sink->display_buf, buf);
  gst_buffer_unref (buf);

  /* wait here for render to complete, so render time is accounted for
   * in latency/jitter calculation:
   */
  while (sink->callback) {
    GST_LOG_OBJECT (sink, "waiting render");
    wl_display_dispatch (sink->display->display);
  }

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gstwayland_debug, "waylandsink", 0,
      " wayland video sink");

  return gst_element_register (plugin, "waylandsink", GST_RANK_MARGINAL,
      GST_TYPE_WAYLAND_SINK);
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "waylandsink",
    "Wayland Video Sink", plugin_init, VERSION, "LGPL", GST_PACKAGE_NAME,
    GST_PACKAGE_ORIGIN)
