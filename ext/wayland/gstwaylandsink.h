/*
 * GStreamer Wayland video sink
 * Copyright (C) 2011 Intel Corporation
 * Copyright (C) 2011 Sreerenj Balachandran <sreerenj.balachandran@intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GST_WAYLAND_VIDEO_SINK_H__
#define __GST_WAYLAND_VIDEO_SINK_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideosink.h>
#include <gst/video/video-crop.h>
#include <gst/drm/gstdrmbufferpool.h>

#include <omap_drm.h>
#include <omap_drmif.h>
#include <wayland-client.h>

#define GST_TYPE_WAYLAND_SINK \
	    (gst_wayland_sink_get_type())
#define GST_WAYLAND_SINK(obj) \
	    (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_WAYLAND_SINK,GstWaylandSink))
#define GST_WAYLAND_SINK_CLASS(klass) \
	    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_WAYLAND_SINK,GstWaylandSinkClass))
#define GST_IS_WAYLAND_SINK(obj) \
	    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_WAYLAND_SINK))
#define GST_IS_WAYLAND_SINK_CLASS(klass) \
	    (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_WAYLAND_SINK))
#define GST_WAYLAND_SINK_GET_CLASS(inst) \
        (G_TYPE_INSTANCE_GET_CLASS ((inst), GST_TYPE_WAYLAND_SINK, GstWaylandSinkClass))

struct touch_point
{
  int32_t id;
  struct wl_list link;
};

struct input
{
  struct display *display;
  struct wl_seat *seat;
  struct wl_pointer *pointer;
  struct wl_touch *touch;
  struct wl_list touch_point_list;
  struct window *pointer_focus;
  struct window *touch_focus;
  struct wl_list link;

  struct window *grab;

};

struct display
{
  struct wl_display *display;
  struct wl_compositor *compositor;
  struct wl_shell *shell;
  struct wl_registry *registry;
  struct wl_drm *drm;
  uint32_t formats[10];
  int format_count;

  struct wl_list input_list;
  int seat_version;
  uint32_t serial;

  /* the drm device.. needed for sharing direct-render buffers..
   * TODO nothing about this should really be omapdrm specific.  But some
   * of the code, like hashtable of imported buffers in libdrm_omap should
   * be refactored out into some generic libdrm code..
   */
  struct omap_device *dev;
  int fd;
  int authenticated;

};

struct window
{
  struct display *display;
  int width, height;
  struct wl_surface *surface;
  struct wl_region *opaque_region;
  struct wl_shell_surface *shell_surface;
  struct wl_buffer *buffer;
};

typedef struct _GstWaylandSink GstWaylandSink;
typedef struct _GstWaylandSinkClass GstWaylandSinkClass;

struct _GstWaylandSink
{

  GstVideoSink parent;

  GstCaps *caps;

  struct display *display;
  struct window *window;
  struct wl_callback *callback;

  GMutex *pool_lock;
  GSList *buffer_pool;
  GstDRMBufferPool *pool;

  GMutex *wayland_lock;

  gint video_width;
  gint video_height;
  /* current displayed buffer and last displayed buffer: */
  GstBuffer *display_buf, *last_buf;

};

struct _GstWaylandSinkClass
{
  GstVideoSinkClass parent;
};

GType
gst_wayland_sink_get_type (void)
    G_GNUC_CONST;

GST_DEBUG_CATEGORY_EXTERN (gstwayland_debug);
#define GST_CAT_DEFAULT gstwayland_debug

G_END_DECLS
#endif /* __GST_WAYLAND_VIDEO_SINK_H__ */
