/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdint.h>
#include <gst/gst.h>
#include <gst/dmabuf/dmabuf.h>
#include <gst/video/video-crop.h>

#include <omap_drm.h>
#include <omap_drmif.h>

#include "gstwaylandsink.h"
#include "gstwlbufferpriv.h"
#include "wayland-drm-client-protocol.h"

#define GST_WL_BUFFER_PRIV_QUARK gst_wl_buffer_priv_quark_get_type()
static GST_BOILERPLATE_QUARK (GstWLBufferPriv, gst_wl_buffer_priv_quark);

#define WL_BUFFER_PRIV_QUARK wl_buffer_priv_quark_get_type()
static GST_BOILERPLATE_QUARK (WLBufferPriv, wl_buffer_priv_quark);

GST_BOILERPLATE_MINI_OBJECT (GstWLBufferPriv, gst_wl_buffer_priv);

static void
set_wl_buffer_priv (GstBuffer * buf, GstWLBufferPriv * priv)
{
  gst_buffer_set_qdata (buf, GST_WL_BUFFER_PRIV_QUARK,
      gst_structure_id_new (GST_WL_BUFFER_PRIV_QUARK,
          WL_BUFFER_PRIV_QUARK, GST_TYPE_WL_BUFFER_PRIV, priv, NULL));
}

static GstWLBufferPriv *
get_wl_buffer_priv (GstBuffer * buf)
{
  const GstStructure *s;
  const GValue *val;

  s = gst_buffer_get_qdata (buf, GST_WL_BUFFER_PRIV_QUARK);
  if (s == NULL)
    return NULL;

  val = gst_structure_id_get_value (s, WL_BUFFER_PRIV_QUARK);
  if (val == NULL)
    return NULL;

  return GST_WL_BUFFER_PRIV (gst_value_get_mini_object (val));
}

static void
gst_wl_buffer_priv_finalize (GstMiniObject * mini_obj)
{
  GstWLBufferPriv *priv = (GstWLBufferPriv *) mini_obj;

  wl_buffer_destroy (priv->buffer);
  omap_bo_del (priv->bo);

  /* not chaining up to GstMiniObject's finalize for now, we know it's empty */
}

static void
gst_wl_buffer_priv_class_init (GstWLBufferPrivClass * klass)
{
  GST_MINI_OBJECT_CLASS (klass)->finalize = gst_wl_buffer_priv_finalize;
}

static int
create_wl_buffer (GstWLBufferPriv * priv, GstWaylandSink * sink,
    GstBuffer * buf)
{
  GstVideoCrop *crop;
  gint video_width = sink->video_width;
  gint video_height = sink->video_height;

  /* TODO get format, etc from caps.. and query device for
   * supported formats, and make this all more flexible to
   * cope with various formats:
   */
  uint32_t fourcc = GST_MAKE_FOURCC ('N', 'V', '1', '2');
  uint32_t name;

  /* note: wayland and mesa use the terminology:
   *    stride - rowstride in bytes
   *    pitch  - rowstride in pixels
   */
  uint32_t strides[3] = {
    GST_ROUND_UP_4 (sink->video_width), GST_ROUND_UP_4 (sink->video_width), 0,
  };
  uint32_t offsets[3] = {
    0, strides[0] * sink->video_height, 0
  };

  crop = gst_buffer_get_video_crop (buf);
  if (crop) {
    guint left = gst_video_crop_left (crop);
    guint top = gst_video_crop_top (crop);
    //offsets[0] += (video_width * top) + left;
    offsets[0] = left;
    offsets[1] += (video_width * top / 2) + left;
    video_width = gst_video_crop_width (crop);
    //video_height = gst_video_crop_height (crop);
  }

  if (omap_bo_get_name (priv->bo, &name)) {
    GST_WARNING_OBJECT (sink, "could not get name");
    return -1;
  }

  priv->buffer = wl_drm_create_planar_buffer (sink->display->drm, name,
      video_width, video_height, fourcc,
      offsets[0], strides[0],
      offsets[1], strides[1],
      offsets[2], strides[2]);

  GST_DEBUG_OBJECT (sink, "create planar buffer: %p (name=%d)",
      priv->buffer, name);

  return priv->buffer ? 0 : -1;
}

GstWLBufferPriv *
gst_wl_buffer_priv (GstWaylandSink * sink, GstBuffer * buf)
{
  GstWLBufferPriv *priv = get_wl_buffer_priv (buf);
  if (!priv) {
    GstDmaBuf *dmabuf = gst_buffer_get_dma_buf (buf);

    /* if it isn't a dmabuf buffer that we can import, then there
     * is nothing we can do with it:
     */
    if (!dmabuf) {
      GST_DEBUG_OBJECT (sink, "not importing non dmabuf buffer");
      return NULL;
    }

    priv = (GstWLBufferPriv *) gst_mini_object_new (GST_TYPE_WL_BUFFER_PRIV);

    priv->bo = omap_bo_from_dmabuf (sink->display->dev,
        gst_dma_buf_get_fd (dmabuf));

    if (create_wl_buffer (priv, sink, buf)) {
      GST_WARNING_OBJECT (sink, "could not create framebuffer: %s",
          strerror (errno));
      gst_mini_object_unref (GST_MINI_OBJECT_CAST (priv));
      return NULL;
    }

    set_wl_buffer_priv (buf, priv);
  }
  return priv;
}
