/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __GSTWLBUFFERPRIV_H__
#define __GSTWLBUFFERPRIV_H__

#include <stdint.h>
#include <gst/gst.h>

#include <omap_drm.h>
#include <omap_drmif.h>

#include <wayland-client.h>

G_BEGIN_DECLS

/*
 * per-buffer private data so that waylandsink can track the wl_buffer
 * associated with a gst buffer.
 */

#define GST_TYPE_WL_BUFFER_PRIV      \
  (gst_wl_buffer_priv_get_type ())
#define GST_WL_BUFFER_PRIV(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_WL_BUFFER_PRIV, GstWLBufferPriv))
#define GST_IS_WL_BUFFER_PRIV(obj)     \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_WL_BUFFER_PRIV))

typedef struct _GstWLBufferPriv      GstWLBufferPriv;
typedef struct _GstWLBufferPrivClass GstWLBufferPrivClass;


struct _GstWLBufferPriv
{
  GstMiniObject parent;

  struct omap_bo *bo;
  struct wl_buffer *buffer;
};

struct _GstWLBufferPrivClass
{
  GstMiniObjectClass parent_class;
};


GType gst_wl_buffer_priv_get_type (void);

GstWLBufferPriv * gst_wl_buffer_priv (GstWaylandSink *sink, GstBuffer * buf);

G_END_DECLS


#endif /* __GSTWLBUFFERPRIV_H__ */
