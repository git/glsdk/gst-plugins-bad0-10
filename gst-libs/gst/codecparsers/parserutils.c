/* Gstreamer
 * Copyright (C) <2011> Intel Corporation
 * Copyright (C) <2011> Collabora Ltd.
 * Copyright (C) <2011> Thibault Saunier <thibault.saunier@collabora.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include "parserutils.h"

gboolean
decode_vlc (GstBitReader * br, guint * res, const VLCTable * table,
    guint length)
{
  guint8 i;
  guint cbits = 0;
  guint32 value = 0;

  for (i = 0; i < length; i++) {
    if (cbits != table[i].cbits) {
      cbits = table[i].cbits;
      if (!gst_bit_reader_peek_bits_uint32 (br, &value, cbits)) {
        goto failed;
      }
    }

    if (value == table[i].cword) {
      SKIP (br, cbits);
      if (res)
        *res = table[i].value;

      return TRUE;
    }
  }

  GST_DEBUG ("Did not find code");

failed:
  {
    GST_WARNING ("Could not decode VLC returning");

    return FALSE;
  }
}

guint
find_mpeg_start_code (const guint8 * data, guint offset, guint size)
{
  const guint8 *start = data;

  g_return_val_if_fail (size > 0, -1);

  /* we can't find the pattern with less than 4 bytes */
  if (G_UNLIKELY (size < 4))
    return -1;

  data += offset;
  size -= 3;
  while (size > 0) {
    const guint8 *ptr = memchr (data, 0, size);
    if (G_UNLIKELY (ptr == NULL))
      return -1;
    if (ptr[2] == 1 && ptr[1] == 0)
      return ptr - start;
    size -= (ptr - data) + 1;
    data = ptr + 1;
  }

  return -1;
}
